<?php
// MyVendor\contactform\src\TU9ServiceProvider.php
namespace Tu9\Tu9GooglemapMarkers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Tu9\Tu9GooglemapMarkers\View\Components\Tu9googleMapMarkers;

class Tu9GmMarkersServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'tu9-googlemap-markers');
        Blade::component('tu9-googlemap-markers', Tu9googleMapMarkers::class);
    }

    public function register()
    {
    }
}
